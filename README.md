# LE VOYAGEUR DE COMMERCE

## Calcule de la matrice des distances
 
On utilise le fichier `exemple.txt` (dans lequel j'ai supprimé le doublon Lyon)

```python
def matrice_distances(liste):
    """
    Matrice des distances entre chaque ville
    : param list: list de ville
    : return matrice tableau contenant les distances entre toutes les villes
    """
    l=len(liste)
    matrice=[[0]*l for i in range(l)]
    for i in range(l):
        for j in range(l):
            matrice[i][j]=distance(liste,i,j)
    return matrice
```

## Détermination de la ville la plus proche d'une ville donnée

```python
def indice_ville_min(i,ville,matrice):
    '''
    :param i entier désignant la position d'une ville
    :param ville liste des villes restantes (valeurs de 0 à len(matrice))
    :param matrice tableau avec les distance entre toutes les villes
    :return indice_min position d'une ville la plus proche de celle à la position i dans ville
    '''
    indice_min=-1
    d=1000
    for j in ville:
        if j!=i:
            if matrice[i][j]<d:
                d=matrice[i][j]
                indice_min=j
    return indice_min
```

## Trajet le plus court à partir d'une ville

```python
def supprime(liste,n):
    '''
    supprime dans la liste l'élément ayant la valeur n
    :param liste liste contenant des entiers dont n
    :param n entier
    :return liste avec l'élément à la position n supprimée
    '''
    rang=0
    for i in range(len(liste)):
        if liste[i]==n:
            rang=i
    liste=liste[:rang]+liste[rang+1:]
    return liste
    
    
def trajet_ville_i(i,matrice):
    '''
    :param i : indice de la ville initiale
    :param matrice : tableau avec toutes les distances
    :return liste des indices des villes réalisant la plus petite distance à partir de la ville d'indice i
    '''
    resultat=[i]
    ville=[j for j in range(len(matrice))]
    ville=supprime(ville,i)
    indice=i
    while len(ville)>0:
        indice=indice_ville_min(indice,ville,matrice)
        ville=supprime(ville,indice)
        resultat.append(indice)
    return resultat

```

## Détermination de toutes les distances des parcours possibles

```python
def conversion_liste_tour(liste,ville):
    '''
    renvoie la liste des villes à partir de celle des indices
    :param liste des positions des villes
    :param liste des villes non ordonnées
    :return liste de villes (nom,coordonnées)
    '''
    res=[]
    for k in liste:
        res.append(ville[k])
    return res

def distances_totales_selon_ville(matrice,ville):
    '''
    :param matrice matrice des distances entre les villes
    :param liste des villes
    :return liste des distances totales posssibles parcourues pour chaque ville
    >>> l=get_tour_fichier('exemple.txt')
    >>> m=matrice_distances(l)
    >>> distances_totales_selon_ville(m,l)
    [56.400230863065644, 56.96607089965117, 66.04146840892494, 51.76324731178717, 50.80306248710208, 47.63007119123532, 46.79152912130661, 56.460263706204984, 52.38375098372309, 54.0459503956192, 54.116547523348466, 63.0598418386992, 53.87310532637897, 68.69269251995745, 60.143336777085565, 47.35397661095585, 59.41301887106276, 70.10099975279192, 47.63007119123532, 60.02193401412255, 62.47648441538496, 55.91160288403023, 56.10536436974894]
    '''
    liste_totale=[]
    for k in range(len(matrice)):
        liste_totale.append(longueur_tour(conversion_liste_tour(trajet_ville_i(k,matrice),ville)))
    return liste_totale

```