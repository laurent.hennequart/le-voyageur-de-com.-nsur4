# coding: UTF-8
import random, numpy, math, pylab, copy


def get_tour_fichier(f):
    """
    :author: DIU-Bloc2 - Univ. Lille
    :date: 2019, Mai
    : lecture fichier de ville format ville, latitude, longitude
    : renvoie un tour contenant les villes dans l ordre du fichier
    : param f: file
    : return : (list)
    """
    fichier = open(f, "r")
    tour_initial = fichier.read().replace (",", ".").split ("\n")
    tour_initial=[ t.strip ("\r\n ").split ("\t") for t in tour_initial ]
    tour_initial = [ t [:1] + [ float (x) for x in t [1:] ] for t in tour_initial ]
    fichier.close()
    return tour_initial[:len(tour_initial)-1]


def distance (tour, i,j) :
    """
    Calcul la distance euclidienne entre i et j
    : param tour: seqeunce de ville
    : param i: numero de la ville de départ
    : param j: numero de la ville d arrivee
    : return: float
    CU: i et j dans le tour
    """
    dx = tour [i][1] - tour [j][1]
    dy = tour [i][2] - tour [j][2]
    return (dx**2 + dy**2) ** 0.5


def longueur_tour (tour) :
    """
    calcul la longueur total d une tournée de la ville de départ et retourne à la ville de départ
    : param tour: tournee de ville n villes = n segments
    : return: float distance totale
    """
    d = 0
    for i in range (0,len(tour)-1) :
        d += distance (tour, i,i+1)
    # il ne faut pas oublier de boucler pour le dernier segment  pour retourner à la ville de départ
    d += distance (tour, 0,-1)
    return d

def trace (tour) :
    """
    Trace la tournée realisée
    : param tour: list de ville
    """
    x = [ t[1] for t in tour ]
    y = [ t[2] for t in tour ] 
    x += [ x [0] ] # on ajoute la dernière ville pour boucler
    y += [ y [0] ] #
    pylab.plot (x,y, linewidth=5)
    for ville,x,y in tour :
        pylab.text (x,y,ville) 
    pylab.show ()

    
def test2():
    """
    Fonction de test des fonctions precedentes
    """
    
    the_tour = get_tour_fichier("exemple.txt")
    print(the_tour)
    print(longueur_tour(the_tour))
    trace(the_tour)
    
def matrice_distances(liste):
    """
    Matrice des distances entre chaque ville
    : param list: list de ville
    : return matrice tableau contenant les distances entre toutes les villes
    """
    l=len(liste)
    matrice=[[0]*l for i in range(l)]
    for i in range(l):
        for j in range(l):
            matrice[i][j]=distance(liste,i,j)
    return matrice

def indice_ville_min(i,ville,matrice):
    '''
    :param i entier désignant la position d'une ville
    :param ville liste des villes restantes (valeurs de 0 à len(matrice))
    :param matrice tableau avec les distance entre toutes les villes
    :return indice_min position d'une ville la plus proche de celle à la position i dans ville
    '''
    indice_min=-1
    d=1000
    for j in ville:
        if j!=i:
            if matrice[i][j]<d:
                d=matrice[i][j]
                indice_min=j
    return indice_min

def supprime(liste,n):
    '''
    supprime dans la liste l'élément ayant la valeur n
    :param liste liste contenant des entiers dont n
    :param n entier
    :return liste avec l'élément à la position n supprimée
    '''
    rang=0
    for i in range(len(liste)):
        if liste[i]==n:
            rang=i
    liste=liste[:rang]+liste[rang+1:]
    return liste
    
    
def trajet_ville_i(i,matrice):
    '''
    :param i : indice de la ville initiale
    :param matrice : tableau avec toutes les distances
    :return liste des indices des villes réalisant la plus petite distance à partir de la ville d'indice i
    '''
    resultat=[i]
    ville=[j for j in range(len(matrice))]
    ville=supprime(ville,i)
    indice=i
    while len(ville)>0:
        indice=indice_ville_min(indice,ville,matrice)
        ville=supprime(ville,indice)
        resultat.append(indice)
    return resultat

def conversion_liste_tour(liste,ville):
    '''
    renvoie la liste des villes à partir de celle des indices
    :param liste des positions des villes
    :param liste des villes non ordonnées
    :return liste de villes (nom,coordonnées)
    '''
    res=[]
    for k in liste:
        res.append(ville[k])
    return res

def distances_totales_selon_ville(matrice,ville):
    '''
    :param matrice matrice des distances entre les villes
    :param liste des villes
    :return liste des distances totales posssibles parcourues pour chaque ville
    >>> l=get_tour_fichier('exemple.txt')
    >>> m=matrice_distances(l)
    >>> distances_totales_selon_ville(m,l)
    [56.400230863065644, 56.96607089965117, 66.04146840892494, 51.76324731178717, 50.80306248710208, 47.63007119123532, 46.79152912130661, 56.460263706204984, 52.38375098372309, 54.0459503956192, 54.116547523348466, 63.0598418386992, 53.87310532637897, 68.69269251995745, 60.143336777085565, 47.35397661095585, 59.41301887106276, 70.10099975279192, 47.63007119123532, 60.02193401412255, 62.47648441538496, 55.91160288403023, 56.10536436974894]
    '''
    liste_totale=[]
    for k in range(len(matrice)):
        liste_totale.append(longueur_tour(conversion_liste_tour(trajet_ville_i(k,matrice),ville)))
    return liste_totale
        